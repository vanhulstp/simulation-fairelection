import Vue from 'vue';
import parties from '@/assets/data/parties.json';

Vue.prototype.$efc = function (candidate) {
    return parties.filter(p => p.id === candidate.groupedparty)[0]?.[`name${this.$root.$i18n.locale}`];
}
