export default class Siege {
    occupant;
    canton;
    party;
    status = 'standard';
    x;
    y;

    constructor({ occupant, party, canton, x, y }) {
        Object.assign(this, { occupant, party, canton, x, y })
    }
}

