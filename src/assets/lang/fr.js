/**
 * La stratégie d'extraction des lignes de texte dans l'application suit la logique suivante :
 * 
 * - On trie toutes les lignes de tous les layouts, ceux-ci par ordre alphabétique ;
 * - On trie toutes les lignes de tous les components, ceux-ci par ordre alphabétique ;
 * - Certains fichiers de données utilisent eux aussi des lignes de texte, qu'on affiche en dernier.
 * 
 * Lorsqu'une ligne de texte revient dans un fichier, on utilise sa définition pré-existante.
 * Vu la taille de l'application, il n'est pas nécessaire de séparer les lignes de textes en plusieurs "sous-domaines".
 */

export default {
    // layouts/defaults.vue
    subtitle: 'Élections Fédérales 2019',
    toFairElection: 'Vers FairElection',
    readyToFair: 'Vous voulez créer une liste avec une meilleure représentation ?',
    tryFair: 'Essayez FairElection',
    address: 'Adresse',
    contact: 'Contact',

    // components/CandidateCard.vue
    newElection: 'Nouvelle élection',
    incumbent: 'Réélection',
    urbanArea: 'Région urbaine',
    ruralArea: 'Région rurale',
    votes: 'Voix',
    ageGroup: 'Groupe d\'âge',
    canton: 'Canton',
    gender: 'Genre',
    female: 'Femme',
    male: 'Homme',

    // components/CantonTable.vue
    candidatesList: 'Liste des candidat·e·s',
    name: 'Nom',
    elected: 'Élu·e',
    party: 'Parti',
    location: 'Domicile',
    age: 'Âge',

    // components/IndexBars.vue
    compareTitle: 'Comparer les différents scénarios pour le canton de {canton}',
    repartitionOfSieges: 'Le canton de {canton} n\'a pas de siège. | Le canton de {canton} n\'a qu\'un seul siège. | Le canton de {canton} a {n} sièges.',
    filterScenarios: 'Choisissez les scénarios à comparer',
    disclaimerData: 'Les statistiques utilisées pour ces représentations sont issues de #[a(href="https://www.bfs.admin.ch/bfs/fr/home.html" target="_blank") l\'Office Fédérale de la Statistique].',

    // components/IndexDescription.vue
    welcome: 'Bienvenue sur le simulateur des élections fédérales 2019 !',
    welcomeText: '<p>A quoi ressemblerait le Conseil national s\'il était représentatif des genres, des âges et des régions (urbain/rural) ? Cet outil propose trois scénarios pour appliquer des critères supplémentaires à l\'élection. Sélectionnez un scénario, puis choisissez votre canton pour voir les différences avec le résultat officiel.</p><p>La simulation se déroule sur la base de la véritable élection de 2019 et des voix reçues par les candidat-es. Le nombre de sièges par partis est fixe, il correspond au résultat de l\'élection. Les critères de genre, d\'âge et de région sont appliqués à l\'échelle du canton. La répartition des âges et des régions dépend de la démographie cantonale (chiffres OFS).</p><p>Des remarques ou des commentaires sur cette simulation ? <a href="mailto:info@fairelection.ch">Dites-le-nous !</a></p><p>Intéressé-e par l\'application de l\'outil "FairElection" ? L\'outil est gratuitement à disposition <a href="https://fairelection.ch">ici</a>.</p>',
    disclaimer: 'Attention',
    disclaimerText: 'Cette simulation ne tient pas compte de tous les mécanismes de l\'élection officielle, par ex. apparentements (les listes apparentées sont considérées comme une liste unique), effets de bord du système proportionnel (différences de voix importantes entre individus de différents partis), personnes ayant renoncé à leur élection, etc. C\'est un outil destiné à ouvrir le débat. Il ne vise pas à se substituer à des système d\'élections officiels.',

    // components/IndexHemisphere.vue
    selectScenario: 'Sélectionnez un scénario pour les Élections fédérales 2019.',
    selectCanton: 'Sélectionnez un canton pour afficher les résultats détaillés.',
    resultsBy: 'Résultats {scenario} | {scenario} résultats | unused',
    allCantons: 'Tous les cantons',
    nationalComposition: 'Dans le scénario sélectionné, le Conseil national comporterait...',

    hemisphereElectedLineOne: 'Nouvellement élu·e', // this sentence is split in three, so that it fits the visualization. Extra care for translation!
    hemisphereElectedLineTwo: 'dans le scénario',
    hemisphereElectedLineThree: 'sélectionné',

    hemisphereRejectedLineOne: 'Personnes', // this sentence is split in three, so that it fits the visualization. Extra care for translation!
    hemisphereRejectedLineTwo: 'non-élues dans',
    hemisphereRejectedLineThree: 'le scénario sélectionné',

    clickASiege: 'Cliquez sur un siège pour afficher les détails de son élu·e.',

    // components/IndexRepartitions.vue
    detailsCanton: 'Détails pour le canton de <em>{canton}</em> | Détails pour le canton de <em>{canton}</em> dans le scénario <em>Résultats {scenario}</em>',
    nbVotesText: 'Ce groupe de personnes élues a récolté ensemble <strong>{totalVotes}</strong> voix. Les personnes élues dans l\'élection officielle ont récolté ensemble <strong>{baseTotalVotes}</strong> voix. La différence entre le scénario officiel et celui-ci est donc de <strong>{differenceVotes}</strong> voix (<strong>{differencePercentage}%</strong>).',
    nbVotesSimilarText: 'Les résultats officiels de l\'élection respectent déjà les critères du scénario sélectionné.',

    // assets/repartitions.js
    siegesByGender: 'Répartition des sièges par genre',
    siegesByLocation: 'Répartition des sièges par domicile',
    siegesByAge: 'Répartition des sièges par âge',

    variableGenderFemales: 'femmes',
    variableGenderMales: 'hommes',
    variableGenderUrban: 'urbain',
    variableGenderRural: 'rural',

    // assets/scenarios.js
    scenarioOfficial: 'officiels',
    scenarioGender: 'avec critère de genre',
    scenarioGenderAge: 'avec critères d\'âge et de genre',
    scenarioLocationAge: 'avec critères d\'âge et de domicile'
}