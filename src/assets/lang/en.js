/**
 * La stratégie d'extraction des lignes de texte dans l'application suit la logique suivante :
 * 
 * - On trie toutes les lignes de tous les layouts, ceux-ci par ordre alphabétique ;
 * - On trie toutes les lignes de tous les components, ceux-ci par ordre alphabétique ;
 * - Certains fichiers de données utilisent eux aussi des lignes de texte, qu'on affiche en dernier.
 * 
 * Lorsqu'une ligne de texte revient dans un fichier, on utilise sa définition pré-existante.
 * Vu la taille de l'application, il n'est pas nécessaire de séparer les lignes de textes en plusieurs "sous-domaines".
 */


 export default {
    // layouts/defaults.vue
    subtitle: '2019 National Council Elections',
    toFairElection: 'To FairElection',
    readyToFair: 'Are you ready to create a more diverse electoral list?',
    tryFair: 'Give FairElection a try',
    address: 'Address',
    contact: 'Contact Us',

    // components/CandidateCard.vue
    newElection: 'New Election',
    incumbent: 'Incumbent',
    urbanArea: 'Urban Area',
    ruralArea: 'Rural Area',
    votes: 'Votes',
    ageGroup: 'Age Group',
    canton: 'Canton',
    gender: 'Gender',
    female: 'Female',
    male: 'Male',

    // components/CantonTable.vue
    candidatesList: 'List of candidates',
    name: 'Name',
    elected: 'Elected',
    party: 'Party',
    location: 'Region',
    age: 'Age',

    // components/IndexBars.vue
    compareTitle: 'Comparison of Results According to Different Representation Criteria',
    repartitionOfSieges: 'No Seat | A Single Seat | Distribution of {n} Seats',
    filterScenarios: 'Review Comparative Scenarios',
    disclaimerData: 'The statistics in this illustration are taken from the #[a(href="https://www.bfs.admin.ch/bfs/de/home.html" target="_blank") Federal Statistical Office].',

    // components/IndexDescription.vue
    welcome: 'Welcome to a simulation of the 2019 Swiss National Council Elections!',
    welcomeText: '<p>What would the Swiss National Council look like if people of different genders, age groups, and regions (urban/rural) were proportionally represented? This simulation presents three scenarios for applying additional criteria to an election. Select a scenario and click on a canton to compare it to the official election result. The simulation is based on the actual 2019 election and the votes that candidates received. The number of seats allocated to each party is fixed and corresponds to the actual election results.</p><p> The criteria of gender, age, and region are applied at the cantonal level. The distribution of ages and regions is determined by the cantonal demographics as specified by the FSO. Do you have notes or feedback regarding this simulation? <a href="mailto:info@fairelection.ch">Please write to us</a>!  </p><p>Interested in how the "FairElection" tool is applied? The tool is available for free <a href="https://fairelection.ch">here</a> .</p>',
    disclaimer: 'Attention',
    disclaimerText: 'This simulation does not take all the mechanisms of the official election into account. E.g., party affiliation (combined electoral lists are treated as a single electoral list), side effects of the system of proportional representation (large disparities in votes cast for candidates from different parties), individuals who have abstained from the election, etc. This simulation is meant to catalyze discussion, not to replace formal electoral systems.',

    // components/IndexHemisphere.vue
    selectScenario: 'Please select a scenario for the 2019 Swiss National Council Election.',
    selectCanton: 'Please select a canton to view detailed results.',
    resultsBy: 'Results {scenario} | {scenario} results | unused}',
    allCantons: 'All Cantons',
    nationalComposition: 'This is how the Swiss National Council would look based on the selected scenario...',

    hemisphereElectedLineOne: 'Elected according to', // this sentence is split in three, so that it fits the visualization. Extra care for translation!
    hemisphereElectedLineTwo: 'the selected',
    hemisphereElectedLineThree: 'scenario',

    hemisphereRejectedLineOne: 'Individuals', // this sentence is split in three, so that it fits the visualization. Extra care for translation!
    hemisphereRejectedLineTwo: 'not elected according',
    hemisphereRejectedLineThree: 'to the selected scenario',

    clickASiege: 'Click on a seat to learn more about the elected individual.',

    // components/IndexRepartitions.vue
    detailsCanton: 'Details for the Canton of <em>{canton}</em> | Details for the Canton of <em>{canton}</em> in this scenario <em>{scenario}</em>',
    nbVotesText: 'This group of elected individuals received a total of <strong>{totalVotes}</strong> votes. The winners of the actual election received a total of <strong>{baseTotalVotes}</strong> votes. The results of the actual election and the selected scenario differed by <strong>{differenceVotes}</strong> votes (<strong>{differencePercentage}%</strong>).',
    nbVotesSimilarText: 'The official election results already fulfill the criteria for the selected scenario.',

    // assets/repartitions.js
    siegesByGender: 'Distribution of Seats by Gender',
    siegesByLocation: 'Distribution of Seats by Region',
    siegesByAge: 'Distribution of Seats by Age',

    variableGenderFemales: 'Females',
    variableGenderMales: 'Males',
    variableGenderUrban: 'Urban',
    variableGenderRural: 'Rural',


    // assets/scenarios.js
    scenarioOfficial: 'Official',
    scenarioGender: 'with the criteria of gender',
    scenarioGenderAge: 'with the criteria of gender and age',
    scenarioLocationAge: 'with the criteria of age and region'
}
