/**
 * La stratégie d'extraction des lignes de texte dans l'application suit la logique suivante :
 * 
 * - On trie toutes les lignes de tous les layouts, ceux-ci par ordre alphabétique ;
 * - On trie toutes les lignes de tous les components, ceux-ci par ordre alphabétique ;
 * - Certains fichiers de données utilisent eux aussi des lignes de texte, qu'on affiche en dernier.
 * 
 * Lorsqu'une ligne de texte revient dans un fichier, on utilise sa définition pré-existante.
 * Vu la taille de l'application, il n'est pas nécessaire de séparer les lignes de textes en plusieurs "sous-domaines".
 */


export default {
    // layouts/defaults.vue
    subtitle: 'Nationalratswahlen 2019',
    toFairElection: 'Zu FairElection',
    readyToFair: 'Sind Sie bereit, eine Liste mit mehr Diversität zu erstellen ?',
    tryFair: 'FairElection ausprobieren',
    address: 'Adresse',
    contact: 'Kontakt',

    // components/CandidateCard.vue
    newElection: 'Neue Wahl',
    incumbent: 'Wiederwahl',
    urbanArea: 'Stadtregion',
    ruralArea: 'Ländliche Region',
    votes: 'Stimmen',
    ageGroup: 'Altersgruppen',
    canton: 'Kanton',
    gender: 'Geschlecht',
    female: 'Frau',
    male: 'Mann',

    // components/CantonTable.vue
    candidatesList: 'Liste der KandidatInnen',
    name: 'Name',
    elected: 'Gewählt',
    party: 'Partei',
    location: 'Region',
    age: 'Alter',

    // components/IndexBars.vue
    compareTitle: 'Vergleich der Ergebnisse mit verschiedenen Repräsentationskriterien',
    repartitionOfSieges: 'Kein Sitz | Ein einziger Sitz | Verteilung von {n} Sitzen',
    filterScenarios: 'Vergleichszenarien Durchgehen',
    disclaimerData: 'Die Statistiken dieser Darstellung stammen aus dem #[a(href="https://www.bfs.admin.ch/bfs/de/home.html" target="_blank") Bundesamt für Statistik].',

    // components/IndexDescription.vue
    welcome: 'Willkommen auf der Simulation der Nationalratswahlen 2019 !',
    welcomeText: '<p>Wie würde der Nationalrat aussehen, wenn die Geschlechter, Altersgruppen und Regionen (städtisch/ländlich) repräsentativ vertreten wären? Dieses Tool stellt drei Szenarien vor, um zusätzliche Kriterien bei einer Wahl anzuwenden. Wählen Sie ein Szenario aus und klicken Sie auf Ihren Kanton, um das Szenario mit dem offiziellen Wahlresultat zu vergleichen. Die Simulation basiert auf der tatsächlichen Wahl von 2019 und den Stimmen, die die KandidatInnen erhalten haben. Die Anzahl an Sitzen pro Partei ist festgelegt und entspricht dem echten Wahlresultat.</p><p> Die Kriterien zu Geschlecht, Alter und Region werden auf kantonaler Ebene angewandt. Die Verteilung der Alter und Regionen hängt von der kantonalen Demographie ab (Angaben des BFS). Bemerkungen oder Kommentare zu dieser Simulation ? <a href="mailto:info@fairelection.ch">Bitte schreibt uns</a>!  </p><p>Interessiert wie das "FairElection" Tool angewendet wird ? Das Tool ist <a href="https://fairelection.ch">hier</a> frei verfügbar.</p>',
    disclaimer: 'Achtung',
    disclaimerText: 'Diese Simulation berücksichtigt nicht alle Mechanismen der offiziellen Wahl, z. B. Parteizugehörigkeit (Listenverbindungen werden als eine einzige Liste betrachtet), Nebeneffekte des Proporzsystems (grosser Unterschied an Stimmen zwischen Personen aus verschiedenen Parteien), Personen, die auf ihre Wahl verzichtet haben, usw. Es ist ein Tool, um eine Diskussion in Gang zu setzten. Es ist nicht dazu gedacht, formale Wahlsysteme zu ersetzen.',

    // components/IndexHemisphere.vue
    selectScenario: 'Wählen Sie ein Szenario der Nationalratswahlen 2019.',
    selectCanton: 'Wählen Sie einen Kanton, um detaillierte Ergebnisse anzuzeigen.',
    resultsBy: 'Ergebnisse {scenario} | {scenario} ergebnisse | unused',
    allCantons: 'Alle Kantone',
    nationalComposition: 'In dem gewählten Szenario würde der Nationalrat wie folgt aussehen...',

    hemisphereElectedLineOne: 'Im ausgesuchten', // this sentence is split in three, so that it fits the visualization. Extra care for translation!
    hemisphereElectedLineTwo: 'Szenario',
    hemisphereElectedLineThree: 'gewählt',

    hemisphereRejectedLineOne: 'Nicht gewählte', // this sentence is split in three, so that it fits the visualization. Extra care for translation!
    hemisphereRejectedLineTwo: 'Personen',
    hemisphereRejectedLineThree: 'im ausgesuchten Szenario',

    clickASiege: 'Klicken Sie auf einen Sitz, um mehr über die gewählte Person zu erfahren.',

    // components/IndexRepartitions.vue
    detailsCanton: 'Details für den Kanton <em>{canton}</em> | Details für den Kanton <em>{canton}</em> im Szenario <em>{scenario}</em>',
    nbVotesText: 'Diese Gruppe gewählter Personen hat insgesamt <strong>{totalVotes}</strong> Stimmen erhalten. Die Wahlsiegenden im offiziellen Wahlgang haben insgesamt <strong>{baseTotalVotes}</strong> Stimmen erhalten. Der Unterschies zwischen dem offiziellen Szenario und diesem beträgt also <strong>{differenceVotes}</strong> Stimmen (<strong>{differencePercentage}%</strong>)',
    nbVotesSimilarText: 'Die offiziellen Wahlergebnisse erfüllen bereits die Kriterien des gewählten Szenarios.',

    // assets/repartitions.js
    siegesByGender: 'Verteilung der Sitze nach Geschlecht',
    siegesByLocation: 'Verteilung der Sitze nach Region',
    siegesByAge: 'Verteilung der Sitze nach Alter',

    variableGenderFemales: 'frauen',
    variableGenderMales: 'männer',
    variableGenderUrban: 'städtisch',
    variableGenderRural: 'ländlich',


    // assets/scenarios.js
    scenarioOfficial: 'offiziell',
    scenarioGender: 'mit Geschlechtskriterien',
    scenarioGenderAge: 'mit Kriterien zu Alter und Geschlecht',
    scenarioLocationAge: 'mit Kriterien zu Alter und Region'
}
