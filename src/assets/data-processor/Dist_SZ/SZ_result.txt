Total votes of the optimal unconstrained committee: 78774
Total votes of an optimal balance committee: 77719
Total number of optimal balance committees: 1
Balance constraints: [[0, 0], [0, 1, 0], [0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 0, 0], [0, 1]]
Votes: [21716 15249 13043 20829  2580  1389  1127  1167   458   411   979   212
   357   319   307  5207  1110  1196   535  6153 14194   211   138   210
  4371  8233  3941  4290  6406 20980  5957  1099   424   247   224   334
   872   302   669   935   300   482   425   438   548   270   284   306
   245   301   205   281   534   346   358   218   401   327   241   433
  1145   825   557   597  1502  1314  1348  1107   152  3602   615   308
   306  5566  1594   490   451   534   787   550   658   326   367   197]

Optimal committee: 1
[46229]Marcel Dettling (SVP): Homme, 30-65, UDC, Rural, votes=21716
[46232]Pirmin Schwander (SVP): Homme, 30-65, UDC, Urbain, votes=20829
[47328]Alois Gmür (CVP): Homme, 30-65, PDC, Urbain, votes=14194
[47923]Petra Gössi (FDP): Femme, 30-65, PLR, Rural, votes=20980
Homme: 3
Femme: 1
18-30: 0
30-65: 4
65+: 0
E-Gauche: 0
Verts: 0
PS: 0
VertsLib: 0
PBD: 0
PEV: 0
PCS: 0
PDC: 1
PLR: 1
UDC: 2
Natio: 0
Autres: 0
Rural: 2
Urbain: 2
