/**
 * Once the Python code has processed all files, this 
 * JS code intends the final steps to generate the 
 * files used in the visualization. 
 */

const ctj = require('csvtojson');
const { performance } = require('perf_hooks');
const fs = require('fs').promises;

const dLog = string => console.log(`Debug: ${string}`, performance.now());

(async function () {
    const json = await ctj({ delimiter: ',' }).fromFile('./candidates.csv');
    const jsonB = await ctj({ delimiter: ';' }).fromFile('./Votes/sd-t-17.02-NRW2019-kandidierende-appendix.csv');
    const elected = jsonB.filter(c => c.flag_gewaehlt === '1');

    elected.forEach(e => {
        e.suggestions = [];
        json.forEach(c => {
            if (c.lastName === e.name && c.firstName === e.vorname) {
                e.suggestions.push(c.IDCandidate);
            }
        })
        if (!e.suggestions.length) console.log(e);
    });

    const formatted = elected.map(e => {
        const id = e.suggestions.length === 1 ? e.suggestions[0] : null;
        return { Name: `[${id}]${e.vorname} ${e.name} (${e.partei_bezeichnung_de})`, '#votes': e.stimmen_kandidat, 'Committee 1': '1', id, suggestions: e.suggestions }
    });

    await fs.writeFile('./scenario_base_towork.json', JSON.stringify(formatted), { encoding: 'utf-8' });

    dLog('Done');
})()
