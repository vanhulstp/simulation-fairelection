Total votes of the optimal unconstrained committee: 305139
Total votes of an optimal balance committee: 273457
Total number of optimal balance committees: 1
Balance constraints: [[0, 0], [0, 3, 0], [0, 1, 1, 1, 0, 0, 0, 3, 1, 2, 0, 0], [1, 3]]
Votes: [ 6269  7429 10982  7178 10539  6791  6090  7217 21264 11135 10601 20871
 17840  8564 11015  8369  1534  1022  1108  1444   963  1307  1544  1525
   319   588   466   366   498   516   304   423   416   753  1470  1171
   979   693   344   702   563  1793   280   834   391   465   386   728
   721   945  1003  1129  1306   813   595   815   726   849   968   651
   248   399   723   249   268   207   162   191   118   139   250   130
   190   364   148  1138   678  1235   672  1421  1232  1446   852  1032
 36044 22882 35577 27597 17111 20536 20806 20636   897 11686 11075 11065
  9016 17067 19701 31363 11005  1056   213   205 37217   432   744  1544
   627 32108 22860 40468 31820 27561 32217 23633 25871 23333 13511 19132
 16068 14024 28325   827 14277 14134 24921   548   330  1299  7291  5085
  1765  1000   426 15904   763   184   833   305 13195   393 10787   294
   212   502   316   201   427   642   321   880   362   483   216   372
   301   304   215   491   244   756   285   594   550   764   683  1128
  2032   618   131   239  1022  2052  1230  2108   701  1401  1429  1179
   586   472   341   539   271  1972   786   724   923  1160  1096  1665
   571   703   328   940   655   348   689  1000   559   306   341  1024
   810  1805   260   282   234   256   262   212   849   385   839   215
   468   267  1331  2402  2597   941  2356  1217  1119  1009   423   747
  1039  1148   418   717  1072   735  1001   721  1956  2042  2127  1758
   559   665]

Optimal committee: 1
[46191]Roland Fischer (glp): Homme, 30-65, VertsLib, Rural, votes=10982
[46198]Michael Töngi (Grüne): Homme, 30-65, Verts, Urbain, votes=21264
[48479]Andrea Gmür (CVP): Femme, 30-65, PDC, Urbain, votes=36044
[48481]Leo Müller (CVP): Homme, 30-65, PDC, Rural, votes=35577
[48613]Prisca Birrer-Heimo (SP): Femme, 30-65, PS, Urbain, votes=31363
[48484]Ida Glanzmann (CVP): Femme, 30-65, PDC, Rural, votes=37217
[48675]Franz Grüter (SVP): Homme, 30-65, UDC, Rural, votes=40468
[48678]Yvette Estermann (SVP): Femme, 30-65, UDC, Urbain, votes=32217
[48686]Albert Vitali (FDP): Homme, 30-65, PLR, Urbain, votes=28325
Homme: 5
Femme: 4
18-30: 0
30-65: 9
65+: 0
E-Gauche: 0
Verts: 1
PS: 1
VertsLib: 1
PBD: 0
PEV: 0
PCS: 0
PDC: 3
PLR: 1
UDC: 2
Natio: 0
Autres: 0
Rural: 4
Urbain: 5
