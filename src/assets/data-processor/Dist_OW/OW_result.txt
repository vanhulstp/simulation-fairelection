Total votes of the optimal unconstrained committee: 5412
Total votes of an optimal balance committee: 5412
Total number of optimal balance committees: 1
Balance constraints: [[0, 0], [0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], [0, 0]]
Votes: [1683 5412 5325  424 1675]

Optimal committee: 1
[48624]Monika Rüegger-Hurschler (SVP): Femme, 30-65, UDC, Rural, votes=5412
Homme: 0
Femme: 1
18-30: 0
30-65: 1
65+: 0
E-Gauche: 0
Verts: 0
PS: 0
VertsLib: 0
PBD: 0
PEV: 0
PCS: 0
PDC: 0
PLR: 0
UDC: 1
Natio: 0
Autres: 0
Rural: 1
Urbain: 0
