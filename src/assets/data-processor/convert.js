/**
 * Once the Python code has processed all files, this 
 * JS code intends the final steps to generate the 
 * files used in the visualization. 
 */

const ctj = require('csvtojson');
const { performance } = require('perf_hooks');
const fs = require('fs').promises;

const base = require('./scenario_base_towork.json');

const dLog = string => console.log(`Debug: ${string}`, performance.now());

(async function () {
    /** All candidates, files from Confederation */
    let bCandidates = [];
    /** All candidates, custom files */
    let candidates = [];

    /** Base scenario with elected candidates */
    const scenarioBase = { all: [] };
    /** Gender scenario with elected candidates */
    const scenarioGender = { all: [] };
    /** Gender - Age scenario with elected candidates */
    const scenarioGenderAge = { all: [] };
    /** Age - Urban scenario with elected candidates */
    const scenarioAgeUrban = { all: [] };

    /** Big dump object. Will host most files' data */
    const bigJ = {};

    dLog('Loading files');
    const dir = await fs.readdir('.');
    await Promise.all(dir.map(async entry => {
        const stats = await fs.lstat(entry);
        if (stats.isDirectory() && entry.includes('Dist_')) {
            bigJ[entry] = {};
            const subDir = await fs.readdir(entry);
            await Promise.all(subDir.map(async f => {
                if (f.includes('.csv')) {
                    const json = await ctj({ delimiter: ',' }).fromFile(`${entry}/${f}`);
                    bigJ[entry][f] = json;
                }
            }));
        } else if (entry.includes('Votes')) {
            const jsonB = await ctj({ delimiter: ';' }).fromFile('Votes/sd-t-17.02-NRW2019-kandidierende-appendix.csv');
            bCandidates.push(...jsonB);
            const json = await ctj().fromFile('candidates.csv');
            candidates.push(...json);
        }

    }))

    dLog('Filtering elected candidates by cantons');
    let bla = 0;
    for (const canton in bigJ) {
        const cantonAbbreviation = canton.substr(5);

        if (bigJ[canton]['scenario_base.csv']) {
            const falsyEntries = bigJ[canton]['scenario_base.csv'].map(candidate => {
                candidate.id = /\[(.*)\]/gm.exec(candidate.Name)[1];
                return candidate;
            });

            const entries = base.filter(entry => falsyEntries.map(e => e.id).includes(entry.id));
            
            scenarioBase.all.push(...entries);
            scenarioBase[cantonAbbreviation] = entries;
        }

        if (bigJ[canton]['scenario_ages_urban.csv']) {
            const entries = bigJ[canton]['scenario_ages_urban.csv'].filter(d => d['Committee 1']).map(d => {
                d.id = /\[(.*)\]/gm.exec(d.Name)[1];
                return d;
            });
            scenarioAgeUrban.all.push(...entries);
            scenarioAgeUrban[cantonAbbreviation] = entries;
        }
        if (bigJ[canton]['scenario_genders_ages.csv']) {
            const entries = bigJ[canton]['scenario_genders_ages.csv'].filter(d => d['Committee 1']).map(d => {
                d.id = /\[(.*)\]/gm.exec(d.Name)[1];
                return d;
            });
            scenarioGenderAge.all.push(...entries);
            scenarioGenderAge[cantonAbbreviation] = entries;
        }
        if (bigJ[canton]['scenario_genders.csv']) {
            const entries = bigJ[canton]['scenario_genders.csv'].filter(d => d['Committee 1']).map(d => {
                d.id = /\[(.*)\]/gm.exec(d.Name)[1];
                return d;
            });
            scenarioGender.all.push(...entries);
            scenarioGender[cantonAbbreviation] = entries;
        }
    }

    dLog('Filtering out candidates who are not elected in any scenario');
    candidates = candidates.filter(c => {

        const scenarios = { scenarioBase, scenarioAgeUrban, scenarioGenderAge, scenarioGender };
        const scenarioArray = [ 'scenarioBase', 'scenarioAgeUrban', 'scenarioGenderAge', 'scenarioGender' ];

        scenarioArray.forEach(s => c[s] = scenarios[s].all.map(d => d.id).includes(c.IDCandidate));

        const elected = scenarioArray.reduce(
            (elected, scenario) => c[scenario] || elected,
            false
        );

        /* Late night, last minute remember: get the votes... excuse the code */
        if (elected) {
            scenarioArray.forEach(s => {
                const index = scenarios[s].all.map(d => d.id).indexOf(c.IDCandidate);
                if (index !== -1) {
                    c.votes = scenarios[s].all[index]['#votes'] || c.votes
                }
            })
        }

        return elected;
    });
    dLog(candidates.length);
    console.log(scenarioBase.all.length)

    dLog('Writing files');
    await fs.writeFile('./elected_candidates.json', JSON.stringify(candidates), { encoding: 'utf-8' });

    dLog('Done');
})()
