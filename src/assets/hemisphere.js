import Siege from '../models/Siege';

function createNodes (numNodes, radius) {
    const nodes = [];
    const width = (radius * 2) + 10;

    for (let i = 0; i < numNodes; i++) {
        const angle = (i / (numNodes)) * Math.PI;
        const x = -(radius * Math.cos(angle)) + (width / 2);
        const y = -(radius * Math.sin(angle)) + (width / 2);
        
        nodes.push(new Siege({ x: x, y: y}));
    }

    return nodes;
}

export default createNodes;