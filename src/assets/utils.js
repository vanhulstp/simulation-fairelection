/**
 * Substract matching elements of an array from another array
 * @param {*} firstArray 
 * @param {*} secondArray 
 */
export function differenceArrays (mainArray, substractArray, filter = 'IDCandidate') {
    return mainArray.filter(entry => {
        const entriesToSubstract = substractArray.map(s => s[filter]);
        return !entriesToSubstract.includes(entry[filter])
    })
}

export function similarityArrays (mainArray, substractArray, filter = 'IDCandidate') {
    return mainArray.filter(entry => {
        const entriesToSubstract = substractArray.map(s => s[filter]);
        return entriesToSubstract.includes(entry[filter])
    })
}