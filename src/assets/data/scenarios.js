import fr from '../lang/fr';
import de from '../lang/de';
import en from '../lang/en';

export default [
    { "id": "scenarioBase", "namede": de.scenarioOfficial, "nameen": en.scenarioOfficial, "namefr": fr.scenarioOfficial },
    { "id": "scenarioGender", "namede": de.scenarioGender, "nameen": en.scenarioGender, "namefr": fr.scenarioGender },
    { "id": "scenarioGenderAge", "namede": de.scenarioGenderAge, "nameen": en.scenarioGenderAge, "namefr": fr.scenarioGenderAge },
    { "id": "scenarioAgeUrban", "namede": de.scenarioLocationAge, "nameen": en.scenarioLocationAge, "namefr": fr.scenarioLocationAge }
]