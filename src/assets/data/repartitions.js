import fr from '../lang/fr';
import de from '../lang/de';
import en from '../lang/en';

const ageGroups = [
    { namede: '18-30', nameen: '18-30', namefr: '18-30', value: 'below 30', bounds: [0,29] },
    { namede: '30-39', nameen: '30-39', namefr: '30-39', value: '30-39', bounds: [30,39] },
    { namede: '40-49', nameen: '40-49', namefr: '40-49', value: '40-49', bounds: [40,49] },
    { namede: '50-59', nameen: '50-59', namefr: '50-59', value: '50-59', bounds: [50,59] },
    { namede: '60-69', nameen: '60-69', namefr: '60-69', value: '60-69', bounds: [60, 69] },
    { namede: '69+', nameen: '69+', namefr: '69+', value: 'above 69', bounds: [70, 150] },
];

const color2 = [ '#00b48b', '#2d43b2' ];
color2.textColor = [ 'white', 'white' ];

const color6 = [ '#00b48b', '#93cad1', '#ccdfe5', '#edf3f8', '#4e89bb', '#2d43b2'];
color6.textColor = [ 'white', 'white', 'black', 'black', 'white', 'white'];

export default [
    { 
        "id": "gender", 
        "icon": "/genre.svg", 
        "namede": de.siegesByGender,
        "nameen": en.siegesByGender,
        "namefr": fr.siegesByGender, 
        "field": "gender", 
        "colors": color2, 
        "variables": [
            { namede: de.variableGenderFemales, nameen: en.variableGenderFemales, namefr: fr.variableGenderFemales, value: 'f' },
            { namede: de.variableGenderMales, nameen: en.variableGenderMales, namefr: fr.variableGenderMales, value: 'm' }
        ]
    },
    { 
        "id": "location",
        "icon": "/lieu.svg",
        "namede": de.siegesByLocation,
        "nameen": en.siegesByLocation,
        "namefr": fr.siegesByLocation,
        "field": "urbanRural",
        "colors": color2,
        "variables": [
            { namede: de.variableGenderUrban, nameen: en.variableGenderUrban, namefr: fr.variableGenderUrban, value: 'urban' },
            { namede: de.variableGenderRural, nameen: en.variableGenderRural, namefr: fr.variableGenderRural, value: 'rural' }
        ]
    },
    { 
        "id": "age",
        "icon": "/age.svg",
        "namede": de.siegesByAge,
        "nameen": en.siegesByAge,
        "namefr": fr.siegesByAge,
        "field": "ageGroup",
        "colors": color6, 
        "variables": ageGroups
    },
]