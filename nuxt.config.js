import de from './src/assets/lang/de';
import en from './src/assets/lang/en';
import fr from './src/assets/lang/fr';

const title = 'FairElection: Élections Fédérales 2019';

export default {
    axios: {
        baseURL: 'http://localhost:3000'
    },
    /*
    ** Headers of the page
    */
    target: 'static',
    head: {
        title,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'twitter:image', name: 'twitter:image', content: 'https://simulation.fairelection.ch/og-image.png' },
            { hid: 'og:image', property: 'og:image', content: 'https://simulation.fairelection.ch/og-image.png' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon-16.ico' },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon-32.ico' },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon-32.png' },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon-96.png' },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon-196.png' },
        ]
    },
    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },
    /*
    ** Global CSS
    */
    css: [
        '@/assets/base.css'
    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '~/plugins/ef2019'
    ],
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
    ],
    srcDir: 'src',
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        'nuxt-i18n'
    ],
    /*
    ** Axios module configuration
    ** See https://axios.nuxtjs.org/options
    */
    axios: {
    },
    i18n: {
        locales: [
            { code: 'fr', iso: 'fr-CH' }, 
            { code: 'de', iso: 'de-CH' },
            { code: 'en', iso: 'en-GB' }
        ],
        baseUrl: 'https://simulation.fairelection.ch',
        defaultLocale: 'fr',
        seo: false,
        vueI18n: {
            fallbackLocale: 'fr',
            messages: { de, fr, en }
        }
    },
    /*
    ** Build configuration
    */
    build: {
        babel: {
            plugins: ['@babel/plugin-proposal-optional-chaining']
        },
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    },
    /*router: {
        base: '/ef2019/'
    }*/
}
