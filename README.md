# FairElection - EF2019

> Small website showing the various results of the Swiss 2019. 


## Editing
This website is built with Nuxt.js, and thus Vue.js. Please refer to their relative documentations for general information:

- [Vue.js docs](https://vuejs.org)
- [Nuxt.js docs](https://nuxtjs.org)

For the sake of convenience, all typical folders usually found at root level in a Nuxt.js project have been moved to ```src``` in this project. To start using the project, install the required dependencies. Browse to the root folder of the project and use the regular installation command:

``` bash
# if using npm
$ npm install
# if using yarn
$ yarn install
```

As this project is meant to be deployed statically on a simple webserver, all changes require to rebuild the whole project. To do so, browse to the root folder of the project and use the build ```generate``` command. This will create a ```dist``` folder at the root folder of your project: this folder can then be served directly through a webserver. 

``` bash
# if using npm
$ npm run generate
# if using yarn
$ yarn generate
```

### Development
To save some precious time during development, use the command ```dev``` to enable hot-reload along with a local webserver. Any modification of the source code will thus be repercuted directly in your browser as you go.

``` bash
# if using npm
$ npm run dev
# if using yarn
$ yarn dev
```

### Translations
This website relies on Nuxt i18n (and thus, Vue i18n) to handle internationalization. 

- [Vue i18n docs](https://kazupon.github.io/vue-i18n/)
- [Nuxt i18n docs](https://i18n.nuxtjs.org/)

For convenience, the language files are located in ```src/assets/lang```. Please make sure to regenerate the website if you change them.

### Data
All data used by the website can be found under ```src/assets/data```. 

- cantons.json: contains the list of Swiss cantons, and their name in French and German;
- elected_candidates.json: contains all candidates and their relative information (demographics, election by scenario, etc);
- parties.json: contains all the parties represented by the candidates;
- repartitions.js: contains the three dimensions used by the scenarios (age, gender, location) along with their possible values;
- scenarios.js: contains all the scenarios;

While most files are self-explanatory and fairly easy to manipulate, ```elected_candidates.json``` is the most complicated file to generate, as it relies on external data. Under ```src/assets/data-processor```, you will find the following items:

- Dist_*: various folders containing the results of the balanced election model for all scenarios, one for each Swiss canton;
- Votes: a folder containing the final results of the 2019 elections;
- candidates.csv: an external data file containing various demographic data regarding candidates;
- convert-base.js: a node script to generate the ```scenario_base_towork.json``` based on the two files mentioned above;
- convert.js: a node script to generate ```elected_candidates.json``` based the cantons' data and ```scenario_base_towork.json```;
- package.json: a NPM file to install required dependencies to process data;
- scenario_base_towork.json: a data file containing all the elected candidates of the 2019 elections.

Please note that ```elected_candidates.json``` contains only those candidates who are elected in at least one scenario of the simulation, as to avoid over 3K of unused candidates' data. If you intend to rebuild the file, you will have to install a local library called ```csvtojson``` to parse CSV files in Node.js. To do so, browse to ```src/assets/data-processor``` and use the following command:

``` bash
# if using npm
$ npm install
# if using yarn
$ yarn install
```

Then, you should be able to generate the results files. In the unlikely case where you need to regenerate the ```scenario_base_towork.json``` file, use the following command from the same folder:

``` bash
$ node convert-base.js
```

You will then need to manually fix candidates that could not be matched against ```candidates.csv```. To do so, search for "null" in the generated document: these candidates are either not found at all (```suggestions``` property is empty) or found several times (```suggestions``` property has more than one value). Find the relevant entry in the ```candidates.csv``` file and fill the "null" fields.

In the case you need to regenerate the ```elected_candidates.json``` file, use the following command:

``` bash
$ node convert.js
```
This should generate various files, including a new ```elected_candidates.json``` in the folder. You can then replace the one in ```src/assets/data``` and regenerate the website.
